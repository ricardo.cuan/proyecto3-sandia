# InkoRun!

Proyecto Final de la materia de Animación Digital y Video juegos. Juego tipo endless runner para PC.

Estudiantes:

- Ricardo Cuan
- María Flores
- Luis Samaniego

## Tabla de contenido
- [Proyecto 3: Bicho Run](#proyecto-3-bicho-run)
  - [Tabla de contenido](#tabla-de-contenido)
  - [Objetivos](#objetivos)
  - [Metodología](#metodología)
  - [One Sheet Document](#one-sheet-document)
    - [Análisis competitivo](#análisis-competitivo)
    - [Punto de venta diferenciador](#punto-de-venta-diferenciador)
  - [Prototipo Funcional](#prototipo-funcional)
    - [Arte](#arte)
    - [Setup o pre-configuración](#setup-o-pre-configuración)
    - [Player](#player)
      - [1. Player: Creación del Player](#1-player-creación-del-player)
      - [2. Player: Controlador del Player](#2-player-controlador-del-player)
        - [2.1. Player: Movimiento del Player](#21-player-movimiento-del-player)
        - [2.2. Player: Salto del Player](#22-player-salto-del-player)
        - [2.3. Player: Mejor salto](#23-player-mejor-salto)
        - [2.4. Player: Mejor detección del suelo](#24-player-mejor-detección-del-suelo)
        - [2.5. Player: Detección de muerte y respawn](#25-player-detección-de-muerte-y-respawn)
        - [2.6. Player: Puntuación y Puntuación más alta](#26-player-puntuación-y-puntuación-más-alta)
      - [3. Player: Animación del Player](#3-player-animación-del-player)
    - [Cámara](#cámara)
      - [1. Cámara estática: Controlador de la cámara](#1-cámara-estática-controlador-de-la-cámara)
    - [Plataformas](#plataformas)
      - [1. Platform: Creación de Plataformas](#1-platform-creación-de-plataformas)
      - [2. Platform: Generación de Plataformas](#2-platform-generación-de-plataformas)
      - [3. Platform: Destrucción de plataformas generadas](#3-platform-destrucción-de-plataformas-generadas)
      - [4. Platform: Aleatoridad en el espaciado](#4-platform-aleatoridad-en-el-espaciado)
      - [5. Platform: Pool de objetos de plataformas](#5-platform-pool-de-objetos-de-plataformas)
      - [6. Platform: Aleatoridad en las plataformas](#6-platform-aleatoridad-en-las-plataformas)
      - [7. Platform: Aleatoridad en las plataformas usando un Pool de objetos](#7-platform-aleatoridad-en-las-plataformas-usando-un-pool-de-objetos)
    - [UI](#ui)
      - [1. UI: Puntuación](#1-ui-puntuación)
      - [2. UI: Menú de Muerte](#2-ui-menú-de-muerte)
      - [3. UI: Pausa](#3-ui-pausa)
      - [4. UI: Menú principal](#4-ui-menú-principal)

## Objetivos
- [X] Realización de un One Sheet Document que sustente las especificaciones del juego a presentar.
- [X] Realizar un prototipo funcional
  - [X] Generar un archivo .exe del proyecto
  - [X] La cámara siga al jugador
  - [X] Pantalla de pausa
  - [X] El jugador pueda reaparecer ya sea por muerte o por posición inadecuada

## Metodología
1. Se corregirá el One Sheet Document de la asignación anterior
2. Se utilizará recursos dado por el docente y recursos de la web
3. Se usará la aplicación Unity en su versión 2020.3.32f1 (LTS)
4. Se realizará el proyecto en conjunto usando un sistema colaborativo. En este caso git, gitlab y git-lfs. El equipo decidió usar gitlab ya que github no aceptaba nuestras credenciales utilizando la red de la universidad
5. Se usará un paquete de assets como un placeholder hasta tener los artes oficiales del bicho suuuuuu.
6. Se grabará la ejecución del proyecto junto con una sustentación.

## One Sheet Document

Endless runner 2D con Cristiano Ronaldo de progatonista, consigue torfeos para obtener power-ups, recolecta balones de oro para sumar puntos y así competir a la mayor puntuación. Cuidado con los jugadores que te harán entradas, si te llegan a golpear quedas fuera.

- **Plataformas**: PC y android
- **Publico objetivo**: Niños y adultos jóvenes
- **N de juadores**: Single player con ranking online
- **Modelo de negocio**: Gratuito con sistema de donación
- **Modos de juego**: Endless

### Análisis competitivo
Desventajas de la competencia
- Temple run
  - Tiene un farmato 3D, lo que hace que se exija al hardware.
  - Tiene texturas sobrecargadas, agota la vista.
- Super mario run
  - No es free to play
  - No se actualiza

### Punto de venta diferenciador
- Cristiano ronaldo como personaje p rincipal
- Facilidad de controles
- Power-ups
- Multiplataforma

## Prototipo Funcional

### Arte
Se decide utilizar el paquete de assets de "Kenney Game Assets all-in-one", específicamente los assets de "Platformer Pack Redux". Los assets de "Kenny Game Assets all-in-one" incluyen assets 2D, assets 3D, audio y tipografía. Este contenido es de dominio público y está libre de uso sin antelación para uso personal y comercial.

Se escoje imagenes de:
- Personaje
  - Idle
  - Corriendo (2)
  - Saltando
- Plataforma (3)
  - Suelo izquierda
  - Suelo centro
  - Suelo derecha

### Setup o pre-configuración
1. Instalación de Unity Hub 3.2.0
2. Instalación de Unity 2020.3.32f1
3. Instalación de Visual Studio Code
4. Instalación de git
   1. Se debe de configurar las credenciales pertinentes
5. Instalación de git-lfs
6. Montaje de repositorio gitlab
   1. Se crea el repositorio y se conecta el proyecto local al repositorio remoto

### Player
Dentro del código, el personaje principal se llamará player.
#### 1. Player: Creación del Player
Se toma el spritesheet del personaje y se realiza los cortes necesario para extraer el sprite del personaje en idle.
1. Almacenar el spritesheet del personaje en "Assets/Art/player_spritesheet.png"
2. Acceder al Sprite Editor en "Windows/2D/Sprite Editor"
3. Seleccionar el archivo "player_spritesheet.png"
4. Realizar la siguiente configuración en el Inspector
   1. **Sprite Mode**: Multiple
   2. **Pixels Per Unit**: 128
   3. **Default/Compression**: None
5. Dentro del Sprite Editor, seleccionar el personaje en idle, teniendo en cuenta que el borde esté dentro del personaje
6. Se nombra a "player_idle" y se le da a apply
7. Se arrastra el archivo "player_idle" a Hierachy. Se reenombra a "Player".

#### 2. Player: Controlador del Player
Se aplica los modificadores correspondientes y el script que lo controla para que el usuario pueda controlar el Player.
1. Aplicar los modificadores
   1. Seleccionar "Player"
   2. Se agrega el componente de:
      1. Box Collider 2D
      2. Rigidbody 2D
         1. **Gravity Scale**: 5
2. Se crea el archivo "Assets/Scripts/PlayerController.cs"

#### 2.1. Player: Movimiento del Player
1. Se abre el archivo "PlayerController" en Visual Studio Code
2. Dentro de la clase PlayerController se coloca
```
  public float velocidad = 5;
  public float salto = 15;
  private Rigidbody2D player;

  void Start() {
    player = GetComponent<Rigidbody2D>();
  }

  void Update() {
    // Desplazamiento horizontal constante
    player.velocity = new Vector2(velocidad, player.velocity.y);
  }
```
Este código hará que el personaje tenga una velocidad de 5, un salto de 15 y que constantemente esté trasladandose a la derecha.

#### 2.2. Player: Salto del Player
1. Se abre el archivo "PlayerController" en Visual Studio Code
2. Dentro de la clase PlayerController, en la sección de declaración de variables se agrega:
```
  public LayerMask GroundLayer;
  private bool isGround;
  private Collider2D playerCollider;
```
3. En Start() se agrega:
```
 playerCollider = GetComponent<Collider2D>();
```
4. En Update() se agrega:
```
  // Salto
  isGround = Physics2D.IsTouchingLayers(playerCollider, GroundLayer);
  if( Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) ){
    if(isGround){
      player.velocity = new Vector2(player.velocity.x, salto);
    }
  }
```
5. En "Layers/Edit Layers" se edita
   1. **User Layer 8**: Ground
   2. **User Layer 9**: Player
6. Se selecciona el Player. En "Inspector/Layer" se selecciona la opción de "Player"
7. Se agrega el archivo "PlayerController.cs" al componente del "Player"
   1. **Ground Layer**: Ground
8. Se crea el archivo de tipo "Physics Material 2D" llamado "Slippy" en la carpeta "Assets/Materials". Los archivos de tipo Physics Material 2D se encuentran en "clic derecho en Materials/Create/2D/Physics Material 2D". Esto es para que el usuario no se estanque en las paredes de la plataforma.
9. Se le asigna un valor de 0 en "Friction"
10. Se arrastra el archivo "Slippy" a la opción de "Material" en el "Box Collider 2D" del "Player".

#### 2.3. Player: Mejor salto
La idea es que se tenga varios niveles de salto, si se deja presionado salta más alto y si se da golpes corto, salta mas bajo.
1. En "Assets/Scripts/PlayerController.cs" y se agrega el siguiente código
```
public float jumpTime = 0.25f;
private float jumpTimeCounter;

void Start() {
    jumpTimeCounter = jumpTime;
}

void Update() {
    // Salto prolongado al dejar presionado el salto
    if( Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0) ) {
        if(jumpTimeCounter > 0) {
            player.velocity = new Vector2(player.velocity.x, salto);
            jumpTimeCounter -= Time.deltaTime;
        }
    }
    if(Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0)){
        jumpTimeCounter = 0;
    }
    if(isGround) {
        jumpTimeCounter = jumpTime;
    }
}

```

#### 2.4. Player: Mejor detección del suelo
1. Crear un objeto vacío en los pies del jugador
2. Abrir el archivo PlayerController.cs
3. Agregar al código

```
public float groundCheckRadius;
public Transform groundCheck;

void Update() {
    isGround = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, GroundLayer);
}
```

#### 2.5. Player: Detección de muerte y respawn
1. Se crea un tag llamado "killbox". Usaremos este tag para detectar la colisión y luego muerte.
2. Se crea un objeto vacío llamado "Catcher" dentro del objeto "MainCamera". Se coloca este objeto por debajo del escenario. Se agrega el componente Box Coler 2D y se edita el valor de size/x a 20.
3. Se crea un archivo ubicado en "Assets/Scripts/GameManager.cs"
```
public class GameManager : MonoBehaviour {

    public Transform platformGenerator;
    private Vector3 platformStartPoint;
    public PlayerController player;

    private Vector3 playerStartPoint;
    private PlatformDestroyer[] platformList;

    void Start() {
        platformStartPoint = platformGenerator.position;
        playerStartPoint = player.transform.position;
    }

    public void RestartGame() {
        /** El método coroutine que corre independientemente de nuestro Script. 
        Este puede agrega algunos delay de tiempo*/
        StartCoroutine("RestartGameCo");

    }

    // IEnumerator es un objeto de Coroutine
    public IEnumerator RestartGameCo() {

        // Inactiva el jugador (lo hace invisible)
        player.gameObject.SetActive(false);

        // Delay
        yield return new WaitForSeconds(0.5f);

        // Resetar los objetos que se trasladan
        player.transform.position = playerStartPoint;
        platformGenerator.position = platformStartPoint;
        player.gameObject.SetActive(true);

        // Plataformas con componente PlatformDestroyer
        platformList = FindObjectsOfType<PlatformDestroyer>();
        for(int i = 0; i < platformList.Length; i++) {
            platformList[i].gameObject.SetActive(false);
        }
    }
}
```
3. Se crea un objeto vacío llamado GameManager y se como componente el código "GameManager.cs". En el componente GameManager se coloca el objeto PlatformGenerator y el objeto Player.
4. En PlayerController se agrega el código de
```
public GameManager gameManager;

// Este es una función nativa de Unity. Se usa para detección de collisiones
void OnCollisionEnter2D(Collision2D other) {
    if(other.gameObject.tag == "killbox") {
        gameManager.RestartGame();
    }
}
```

#### 2.6. Player: Puntuación y Puntuación más alta



#### 3. Player: Animación del Player
1. Seleccionar el archivo "player_spritesheet.png"
2. Dentro del Sprite Editor, seleccionar el personaje en salto, teniendo en cuenta que el borde esté dentro del personaje. Se nombra a "player_jump" y se le da a apply.
3. Dentro del Sprite Editor, seleccionar el personaje corriendo, en este caso se tiene dos archivos del personaje corriendo. Se nombra "player_run_1" y "player_run_2".
4. Se abre la ventana de Animation en "Window/Animation/Animation"
5. Se selecciona el objeto "Player". En Animation, se le da a crear nuevo clip y se almacena en "Assets/Animations/player_idle.anim" usando el archivo de "player_idle".
6. Se crea un segundo clip llamado "player_jump.anim" usando el archivo de "player_jmup".
7. Se crea un tercer clip llamado "player_run.anim" usando los archivos de "player_run_1" y "player_run_2". En total el clip dura 10 frames
8. Se abre la ventana de Animator en "Window/Animation/Animator"
9. Se agrega los parámetros en "Patameters"
   1.  float "speed"
   2.  bool "isGround"
10. Se hacen conexiones de transición en ambas vías entre:
    1.  player_idle y player_run
    2.  player_run y player_jump
11. Cada transición tendrá la siguiente configuración en el inspector:
    1.  **Has Exit Time**: false
    2.  **Transition Duration**: 0
12. En "Inspector/Conditions" colocar
    1.  player_idle a player_run:
        1.  speed great 0
        2.  isGronud true
    2.  player_run a player_idle:
        1.  speed less 0.1
        2.  isGround true
    3.  player_run a player_jump
        1.  isGround false
    4.  player_jump a player_run
        1.  is ground true
13. Se abre en VSCode el archivo "Assets/Scripts/PlayerController"
    1.  En la declaración de variables se agrega:
        ```private Animator playerAnimator;```
    2.  En Start() se agrega:
        ```playerAnimator = GetComponent<Animator>();```
    3.  En Update() se agrega:
        ```// Animator
        playerAnimator.SetFloat("speed", player.velocity.x);
        playerAnimator.SetBool("isGround", isGround);```

Como resultado, se tiene el archivo PlayerController.cs
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float velocidad = 5;
    public float salto = 15;
    public LayerMask GroundLayer;

    private bool isGround;
    private Rigidbody2D player;
    private Collider2D playerCollider;
    private Animator playerAnimator;


    // Start is called before the first frame update
    void Start() {
        player = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<Collider2D>();
        playerAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        // Desplazamiento horizontal constante
        player.velocity = new Vector2(velocidad, player.velocity.y);

        // Salto
        isGround = Physics2D.IsTouchingLayers(playerCollider, GroundLayer);
        if( Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) ){
            if(isGround){
                player.velocity = new Vector2(player.velocity.x, salto);
            }
        }

        // Animator
        playerAnimator.SetFloat("speed", player.velocity.x);
        playerAnimator.SetBool("isGround", isGround);
    }
}
```

### Cámara
#### 1. Cámara estática: Controlador de la cámara
1. Se crea el archivo "Assets/Scripts/CameraController.cs"
2. Se agrega el siguiente código al archivo CameraController usando VSCode
```
public class CameraController : MonoBehaviour {

    public PlayerController player;
    private Vector3 lastPlayerPosition;
    private float distanceToMove;

    void Start() {
        player = FindObjectOfType<PlayerController>();
        lastPlayerPosition = player.transform.position;
    }

    void Update() {
        // Cálculo del delta X del jugador
        distanceToMove = player.transform.position.x - lastPlayerPosition.x;

        // traslada la posición de la cámara a la nueva posición
        transform.position = new Vector3(
            transform.position.x + distanceToMove,
            transform.position.y,
            transform.position.z
        );

        // Nueva última posición
        lastPlayerPosition = player.transform.position;
    }
}
```
El código constantemente está calculando la posición del jugador y la cámara se traslada a la nueva posición. Los cálculos que se realizan son de resta y suma, por lo que es de baja computación.

3. Se agrega el archivo "CameraController" al componente del objeto "Main Camera". Se arrastra el objeto "Player" en el "Inspector/Camera Controller/Player".

### Plataformas
#### 1. Platform: Creación de Plataformas
1. Se toma el spritesheet de la plataforma y se realiza los cortes necesario para extraer el sprite del personaje en idle.
   1. Almacenar el spritesheet de las plataformas en "Assets/Art/platform_spritesheet.png"
   2. Seleccionar el archivo "platform_spritesheet.png"
   3. Realizar la siguiente configuración en el Inspector
      1. **Sprite Mode**: Multiple
      2. **Pixels Per Unit**: 128
      3. **Default/Compression**: None
   4. Dentro del Sprite Editor, seleccionar "Slice/type/Grid By Cell Size" y se coloca 128, ya que esta es la distancia de los tiles (128x128). Se le da a apply
2. En Hierachy se crea un objeto vacío con el nombre de "Platform7x1"
3. Arrastre los tiles necesario para realizar una plataforma 7x1, en este caso tiles del mismo tipo dónde uno sea de la izquierda, centro y derecha.
4. Se agrega el componente de "BoxCollider 2D" y se edita el size de x=7 y y=1.
5. Se colocan dos plataformas 7x1 para que el jugador pueda caer y saltar

#### 2. Platform: Generación de Plataformas
1. En Herarchy, se crea un objeto vacío en "Main Camera" llamado "PlatformGenerationPoint". Este objeto vacío se traslada a 20 unidades a la derecha de la cámara. Lo que se quiere es que se genere las plataformas entre este punto y el Player.
2. Se crea el archivo "Assets/Scripts/PlatformGenerator.cs" y se coloca el siguiente código usando VSCode
```
public class PlatformGenerator : MonoBehaviour {

    public GameObject platform;
    public Transform generationPoint;
    public float distanceBetween = 1.5;

    private float platformWidth;

    void Start() {
        platformWidth = platform.GetComponent<BoxCollider2D>().size.x;
    }

    void Update() {
        if( transform.position.x < generationPoint.position.x ) {
            transform.position = new Vector3(
                transform.position.x + platformWidth + distanceBetween,
                transform.position.y,
                transform.position.z
            );
            Instantiate( platform, transform.position, transform.rotation );
        }
    }
}
```
3. En Herachy, se cera un objeto vacío llamado "PlatformGenerator". A este objeto se le agrega como componente el archivo "PlatformGenerator.cs". Este objeto se traslada al centro de la última plataforma.

#### 3. Platform: Destrucción de plataformas generadas
1. En Herarchy, se crea un objeto vacío en "Main Camera" llamado "PlatformDestructionPoint". Este objeto vacío se traslada a 10 unidades a la izquierda de la cámara. Lo que se quiere es que se destruya las plataformas una vez pasado este punto.
2. Crear un archivo llamado "Assets/Scripts/PlatformDestroyer.cs" y se crea el siguiente código
```
public class PlatformDestroyer : MonoBehaviour {

    public GameObject platformDestructionPoint;

    void Start() {
        platformDestructionPoint = GameObject.Find("PlatformDestructionPoint");
    }

    void Update() {
        if( transform.position.x < platformDestructionPoint.transform.position.x ){
            Destroy( gameObject );
        }
    }
}
```
4. Este archivo se agrega como componente a las plataformas.
5. Se crea una carpeta llamada "Assets/Prefabs"
6. Se arrastra el objeto "Platform7x1" dentro de la carpeta "Prefabs"
7. Seleccionar el objeto PlatformGenerator y arrastrar el Prefab Platform7x1 al objeto Platform

#### 4. Platform: Aleatoridad en el espaciado
1. En PlatformGenerator.cs se agrega en la declaración de variables
```
    public float distanceBetweenMin = 1;
    public float distanceBetweenMax = 3;
    private float distanceBetween;
```
2. En PlatformGenerator.cs se modifica a Void()
```
if( transform.position.x < generationPoint.position.x ) {

            distanceBetween = Random.Range( distanceBetweenMin, distanceBetweenMax );

            transform.position = new Vector3(
                transform.position.x + platformWidth + distanceBetween,
                transform.position.y,
                transform.position.z
            );
            Instantiate( platform, transform.position, transform.rotation );
        }
```

#### 5. Platform: Pool de objetos de plataformas
La idea es que en vez de que Unity tenga que crear el objeto y luego eliminarlo, se cree una cantidad de plataformas, luego se active y desactive para así reutilizarlo cuando se haga falta. Esto mejora el uso de procesamiento ya que no tenemos que llamar al recolector de basura de Unity y el uso de memoria. Esto nos ayudará ya que el juego está orientado a estar en Android.

1. Se crea un archivo llamado "Assets/Scripts/ObjectPool.cs"
```
public class ObjectPooler : MonoBehaviour {

    public GameObject pooledObject;
    public int pooledAmount = 10;

    List<GameObject> pooledObjects;

    void Start() {
        pooledObjects = new List<GameObject>();

        for(int i=0; i < pooledAmount; i++) {
            GameObject obj = (GameObject)Instantiate(pooledObject);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
    }

    public GameObject GetPooledObject() {
        for(int i = 0; i < pooledObjects.Count; i++) {
            if( !pooledObjects[i].activeInHierarchy ){
                return pooledObjects[i];
            }
        }
        GameObject obj = (GameObject)Instantiate(pooledObject);
        obj.SetActive(false);
        pooledObjects.Add(obj);
        return obj;
    }
}
```
2. Se crea un objeto vacío llamado "ObjectPooler". Se agrega el componente con código de ObjectPooler.cs
3. Se desactiva la instanciación de objetos en el PlatformGenerator.cs y en su vez, se coloca el siguiente código
```
public ObjectPooler objectPool;
void update() {
    GameObject newPlatform = objectPool.GetPooledObject();
    newPlatform.transform.position = transform.position;
    newPlatform.transform.rotation = transform.rotation;
    newPlatform.SetActive(true);
}
```
4. Se desactiva la destrucción de objetos en el PlatformDestroyer.cs y en su vez, se coloca el siguiente código
```
gameObject.SetActive(false);
```
5. Al correr el código, nos damos cuenta que la cantidad óptima de plataforma a colocar es de 7.

#### 6. Platform: Aleatoridad en las plataformas
1. Se crean nuevas plataformas con las siguientes dimensiones
   1. Platform3x1
   2. Platform5x1
   3. Platform9x1
2. En "Assets/Scripts/PlatformGenerator.cs" agregar:
```
public GameObject[] platform;
private float[] platformWidths;
private int platformSelector;

void Start() {
    platformWidths = new float[platforms.Length];
    for (int i = 0; i < platforms.Length; i++) {
        platformWidths[i] = platforms[i].GetComponent<BoxCollider2D>().size.x;
    }
}

void Update() {
    platformSelector = Random.Range(0, platform.length);
    transform.position = new Vector3(
        transform.position.x + platformWidths[platformSelector] + distanceBetween,
        transform.position.y,
        transform.position.z
    );

    Instantiate( /*platform*/ platforms[platformSelector], transform.position, transform.rotation );
}

```
3. Seleccionar el objeto "PlatformGenerator". En "Inspector / PlatformGenerator / platforms" agregar los prefab de las plataformas.


#### 7. Platform: Aleatoridad en las plataformas usando un Pool de objetos

Una vez que se tiene las plataformas generando aleatoriamente, se reemplaza el arreglo de plataformas por un arreglo de ObejctPools.

1. En el archivo "Assets/Scripts/PlatformGenerator.cs" editar a
```
public GameObject platform;
    // public GameObject[] platforms;
    public ObjectPooler[] objectPools;
    public Transform generationPoint;
    public float distanceBetweenMin = 0.5f;
    public float distanceBetweenMax = 3f;

    private float[] platformWidths;
    private float distanceBetween;
    private float platformWidth;
    private int platformSelector;

    void Start() {
        // platformWidth = platform.GetComponent<BoxCollider2D>().size.x;
        platformWidths = new float[objectPools.Length];
        for (int i = 0; i < objectPools.Length; i++) {
            platformWidths[i] = objectPools[i].pooledObject.GetComponent<BoxCollider2D>().size.x;
        }
    }

    void Update() {
        if( transform.position.x < generationPoint.position.x ) {

            distanceBetween = Random.Range( distanceBetweenMin, distanceBetweenMax );
            platformSelector = Random.Range(0, objectPools.Length);

            // Mover el objeto actual a la nueva posición
            transform.position = new Vector3(
                transform.position.x + platformWidths[platformSelector]/2 + distanceBetween,
                transform.position.y,
                transform.position.z
            );

            // Instantiate( /*platform*/ platforms[platformSelector], transform.position, transform.rotation );

            // Activar una nueva plataforma del pool de objeto
            GameObject newPlatform = objectPools[platformSelector].GetPooledObject();

            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);

            // Colocar el centro de la plataforma al final
            transform.position = new Vector3(
                transform.position.x + platformWidths[platformSelector]/2,
                transform.position.y,
                transform.position.z
            );
        }
    }
```
2. Crear un objeto vacío llamado "ObjectPools". Dentro de este objeto se arrastrará el objeto ya creado llamado "ObjectPooler" y se duplicará este objeto a la cantidad de Prefabs que se tenga, luego se le asignará a cada uno en "Inspector/ObjectPooler/PooledObject" el prefab correspondiente

### UI

#### 1. UI: Puntuación

1. Crear un objeto de texto llamado ScoreText. Trasladarlo a la esquina superior izquierda de la pantalla. Se coloca un texto "Score: 0" y un tamaño de 24.
2. Se duplica el objeto ScoreText y se renombra a HighScoreText y se coloca en la esquina superior derecha. Se coloca un texto de "High Score: 0".
3. Dentro del objeto Canvas (Este objeto se genera automáticamente al crear los objetos de texto) se crea un objeto vacío llamado ScoreManager
4. Se crea un nuevo archivo ubicado en "Assets/Scripts/ScoreManager.cs". y se agrega este archivo como componente de ScoreManager.
```
public class ScoreManager : MonoBehaviour {

    public Text scoreText;
    public Text highScoreText;
    public float scoreCount = 0;
    public float highScoreCount = 0;
    public float pointsPerSecond = 5;
    public bool  scoreIncreasing = true;

    void Start() {
        if(PlayerPrefs.HasKey("HighScore")){
            highScoreCount = PlayerPrefs.GetFloat("HighScore");
        }
    }

    void Update() {

        if(scoreIncreasing) {
            scoreCount += pointsPerSecond * Time.deltaTime;
        }

        if(scoreCount > highScoreCount) {
            highScoreCount = scoreCount;

            // Persistencia de dato
            PlayerPrefs.SetFloat("HighScore", highScoreCount);
        }

        // Mostrar contadores
        scoreText.text = "Score: " + Mathf.Round( scoreCount );
        highScoreText.text = "High Score: " + Mathf.Round( highScoreCount );
    }
}
```
5. Se agrega el siguiente código en "Assets/Scripts/.cs"
```
private ScoreManager scoreManager;

void Start() {
    scoreManager = FindObjectOfType<ScoreManager>();
}

public IEnumerator RestartGameCo() {
    // Congelar la puntuación
    scoreManager.scoreIncreasing = false;

    // Delay
    yield return new WaitForSeconds(0.5f);

    // Resetear la puntuación
    scoreManager.scoreCount = 0;
    scoreManager.scoreIncreasing = true;
}

```

#### 2. UI: Menú de Muerte

1. Se crea un objeto vacío del objeto Canvas llamado "DeathMenu"
2. Se crea un UI/Image Dentro de "DathMenu". Se cambia el color a negro. Se modifica el valor del alfa a 100. Se escala el objeto para que ocupe toda la pantalla.
3. Se crea un Text dentro de "DathMenu" llamado "DeathText". En text se colocará "Has muerto!".
4. Se crea un Button dentro de "DathMenu" llamado "RestartButton". En text se colocará "Nuevo intento".
5. Se crea un script llamado "DeathMenu.cs"
```
public void RestartGame() {
    FindObjectOfType<GameManager>().Reset();
}
```
6. En RestartButton, se le asigna un onClick usando el objeto DeathMenu y la función RestartGame

#### 3. UI: Pausa

1. Se crea un objeto vacío del objeto Canvas llamado "PauseMenu"
2. Se crea un UI/Image Dentro de "PauseMenu". Se cambia el color a negro. Se modifica el valor del alfa a 100. Se escala el objeto para que ocupe toda la pantalla.
3. Se crea un Text dentro de "PauseMenu" llamado "PauseText". En text se colocará "PAUSA".
4. Se crea un Button dentro de "PauseMenu" llamado "ResumeButton". En text se colocará "Reanudar".
5. Se crea un Button dentro de "PauseMenu" llamado "RestartButton". En text se colocará "Nuevo intento".
6. Se crea un nuevo Button en el objeto canvas con nombre de "PauseButton" y texto de "Pausa".
7. Se crea un Script llamado "PauseMenu.cs"
```
public GameObject pauseMenu;

    public void PauseGame() {
        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
    }

    public void ResumeGame() {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
    }

    public void RestartGame() {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        FindObjectOfType<GameManager>().Reset();
    }
```
9. Se coloca el script del PauseMenu como componente del objeto PauseButton

#### 4. UI: Menú principal

1. Se crea una nueva escena llamada MainMenuScene
2. En File/Build Settings... arrastrar las dos escenas y colocar primero la escena de MainMenuScene
3. En MainMenuScene:
   1. Se agrega una Image con color azul oscuro y escalado de 50x50.
   2. Se agrega un Text llamado TitleText con el texto del título del juego.
   3. Se agrega un ButtEon llamado PlayButton con texto de "EMPEZAR".
   4. Se agrega un Button llamado QuitButton con texto de "SALIR DEL JUEGO".
4. Se crea el archivo Assets/Scripts/MainMenu.cs y se agrega como componente al objeto Canvas.
```
public string mainGame;

public void PlayGame() {
    Application.LoadLevel(mainGame);
}

public void QuitGame() {
    Application.Quit();
}
```
5. En el botón PlayButton se configura el OnClic() y se inserta el objeto Canvas con la función de MainMenu/PlayGame
6. En el botón QuitButton se configura el OnClic() y se inserta el objeto Canvas con la función de MainMenu/QuitGame

### Trofeos / Monedas

#### 1. Creación del trofeo

1. Se importa al proyecto el spritesheet de estos recursos
2. En este caso se realiza una configuración de:
   1. Pixels per Unit: 128
   2. Filter Mode: Point
   3. Max Size: 1024
   4. Compression: None
3. Se agrega el objeto a la escena
4. Se aplica los siguientes Componentes
   1. Circle Collider 2D
5. Se crea el archivo "Assets/Scripts/pickupPoints" y colocar el siguinete código:
```c#
public class pickupPoints : MonoBehaviour {

    public int scoreToGive = 100;
    private ScoreManager scoreManager;

    void Start() {
        scoreManager = FindObjectOfType<ScoreManager>();
    }

    void Update() {
        
    }

    // Función nativa de unity. Si dos collider se juntan, se activa esta función
    void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.name == "Player") {
            scoreManager.AddScore(scoreToGive);
            gameObject.SetActive(false);
        }
    }
}
```
6. Se agrega este código como componente a la moneda

#### 2. Generación aleatoria del trofeo

Se busca la creación de una pool de objetos en este caso del objeto moneda.
1. Crear un objeto vacío llamado "CoinGenerator". Dentro de este objeto se crea un segundo objeto vacío llamado "CoinPool". A este último objeto se le agrega el componente de "Object Pooler" ya creado. En este componente se agrega el coin como Pooled Object y la cantidad de Pooled pondremos 15.
2. Se crea el archivo "Assets/Scripts/CoinGenerator.cs" y se agrega el siguiente código
```
public class CoinGenerator : MonoBehaviour {

    public ObjectPooler coinPool;
    // Se busca que se genere 3 monedas en la plataforma.
    public float distanceBetweenCoins;

    private void spawnCoin(Vector3 startPosition, float distanceX) {
        GameObject coin = coinPool.GetPooledObject();
        coin.transform.position = new Vector3(startPosition.x + distanceX, startPosition.y, startPosition.z);
        coin.SetActive(true);
    }

    public void SpawnOneCoins(Vector3 startPosition) {
        spawnCoin(startPosition, 0);
    }

    public void SpawnTwoCoins(Vector3 startPosition) {
        spawnCoin(startPosition, 0);
        spawnCoin(startPosition, distanceBetweenCoins);
    }

    public void SpawnThreeCoins(Vector3 startPosition) {
        spawnCoin(startPosition, -1*distanceBetweenCoins);
        spawnCoin(startPosition, 0);
        spawnCoin(startPosition, distanceBetweenCoins);
    }
}
```

#### 3. Eliminación de los trofeos

1. Se reutilizará el código del PlatformDestroyer. Se agrega como componente el "PlatformDestroyer" en el objeto "Coin".

### Audio

1. Se investigan efectos de sonido para el Salto, la Muerte, Moneda y PowerUp
2. Se crea un folder llamado "Assets/Audio"
3. Se crea un objeto vacío llamado "SoundEffect". En este objeto se agregan los efectos de sonidos. A estos archivos se desactiva la opción de "Play On Awake"
4. Se agrega el archivo de sonido en el momento que se necesite escuchar usando la función Play().

### Enemy

#### Spikes

1. Se importa el arte del Spike.
2. Al Spike se le agrega el tag "Killbox" creado anteriormente y el componente Platform Destroyer.
3. Se procede a crear también un pool de objetos. Se crea un objeto vacío llamado "SpikePool". En este objeto se agregará el componente Objecto Pooler.
4. Dentro de Platform Generator se crea la lógica de la generación de la misma, usando el SpikePool.

### PowerUps

#### x2 de puntos

#### Quitar todos los enemigos
