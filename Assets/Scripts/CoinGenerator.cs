using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinGenerator : MonoBehaviour {

    public ObjectPooler coinPool;
    // Se busca que se genere 3 monedas en la plataforma.
    public float distanceBetweenCoins;

    private void spawnCoin(Vector3 startPosition, float distanceX) {
        GameObject coin = coinPool.GetPooledObject();
        coin.transform.position = new Vector3(startPosition.x + distanceX, startPosition.y, startPosition.z);
        coin.SetActive(true);
    }

    public void SpawnOneCoins(Vector3 startPosition) {
        spawnCoin(startPosition, 0);
    }

    public void SpawnTwoCoins(Vector3 startPosition) {
        spawnCoin(startPosition, 0);
        spawnCoin(startPosition, distanceBetweenCoins);
    }

    public void SpawnThreeCoins(Vector3 startPosition) {
        spawnCoin(startPosition, -1*distanceBetweenCoins);
        spawnCoin(startPosition, 0);
        spawnCoin(startPosition, distanceBetweenCoins);
    }
}
