using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public Transform platformGenerator;
    private Vector3 platformStartPoint;
    public PlayerController player;

    private Vector3 playerStartPoint;
    private PlatformDestroyer[] platformList;
    private ScoreManager scoreManager;

    public DeathMenu deathScreen;

    private AudioSource principalMusic;

    public bool powerupReset;

    void Start() {
        platformStartPoint = platformGenerator.position;
        playerStartPoint = player.transform.position;

        scoreManager = FindObjectOfType<ScoreManager>();

        principalMusic = GameObject.Find("principalMusic").GetComponent<AudioSource>();
    }

    void Update() {
        
    }

    public void RestartGame() {
        /** El método coroutine que corre independientemente de nuestro Script.
        Este puede agrega algunos delay de tiempo*/
        // StartCoroutine("RestartGameCo");

        principalMusic.Stop();

        // Congelar la puntuación
        scoreManager.scoreIncreasing = false;

        // Inactiva el jugador (lo hace invisible)
        player.gameObject.SetActive(false);

        deathScreen.gameObject.SetActive(true);
    }

    public void Reset() {
        // Quitar las pantallas
        deathScreen.gameObject.SetActive(false);

        // Resetar los objetos que se trasladan
        player.transform.position = playerStartPoint;
        platformGenerator.position = platformStartPoint;
        player.gameObject.SetActive(true);

        // Plataformas con componente PlatformDestroyer
        platformList = FindObjectsOfType<PlatformDestroyer>();
        for(int i = 0; i < platformList.Length; i++) {
            platformList[i].gameObject.SetActive(false);
        }

        // Resetear la puntuación
        scoreManager.Reset();
        scoreManager.scoreIncreasing = true;

        principalMusic.Play();
        powerupReset = true;
    }

    /*// IEnumerator es un objeto de Coroutine
    public IEnumerator RestartGameCo() {

        // Congelar la puntuación
        scoreManager.scoreIncreasing = false;

        // Inactiva el jugador (lo hace invisible)
        player.gameObject.SetActive(false);

        // Delay
        yield return new WaitForSeconds(0.5f);

        // Resetar los objetos que se trasladan
        player.transform.position = playerStartPoint;
        platformGenerator.position = platformStartPoint;
        player.gameObject.SetActive(true);

        // Plataformas con componente PlatformDestroyer
        platformList = FindObjectsOfType<PlatformDestroyer>();
        for(int i = 0; i < platformList.Length; i++) {
            platformList[i].gameObject.SetActive(false);
        }

        // Resetear la puntuación
        scoreManager.scoreCount = 0;
        scoreManager.scoreIncreasing = true;

    }*/
}
