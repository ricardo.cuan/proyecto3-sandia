using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public string mainGame;
    private AudioSource menuMusic;

    public void PlayGame() {
        menuMusic = GameObject.Find("MenuMusic").GetComponent<AudioSource>();
        menuMusic.Play();
        SceneManager.LoadScene(mainGame);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
