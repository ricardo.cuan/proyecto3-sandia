using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Text scoreText;
    public Text highScoreText;
    public Text levelText;
    public float scoreCount = 0;
    public float highScoreCount = 0;
    public float pointsPerSecond = 5;
    public bool  scoreIncreasing = true;
    public bool shouldDouble = false;
    public float levelScore = 100;
    public float levelScoreIncrement = 50;
    private float levelScoreStorage;
    public int level = 1;

    public GameObject bg_lvl1;
    public GameObject bg_lvl2;

    void Start() {
        levelScoreStorage = levelScore;

        if(PlayerPrefs.HasKey("HighScore")){
            highScoreCount = PlayerPrefs.GetFloat("HighScore");
        }
    }

    void Update() {

        // Incremento de la puntuación
        if(scoreIncreasing) {
            scoreCount += pointsPerSecond * Time.deltaTime;
        }

        // Incremento de nivel
        if(scoreCount > levelScore) {
            level++;
            levelScore += levelScoreIncrement*2;
            bg_lvl2.SetActive(true);
            bg_lvl1.SetActive(false);
        }

        // Nuevo record
        if(scoreCount > highScoreCount) {
            highScoreCount = scoreCount;

            // Persistencia de dato
            PlayerPrefs.SetFloat("HighScore", highScoreCount);
        }

        // Mostrar contadores
        scoreText.text = "Puntos: " + Mathf.Round( scoreCount );
        highScoreText.text = "Récord: " + Mathf.Round( highScoreCount );
        levelText.text = "lvl. " + level;

    }

    public void Reset() {
        scoreCount = 0;
        level = 1;
        levelScore = levelScoreStorage;

        // BG reset
        bg_lvl1.SetActive(true);
        bg_lvl2.SetActive(false);
    }

    public void AddScore(int pointsToAdd) {
        if(shouldDouble){
            pointsToAdd = pointsToAdd * 2;
        }
        scoreCount += pointsToAdd;
    }
}
