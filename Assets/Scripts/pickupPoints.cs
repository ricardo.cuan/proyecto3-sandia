using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickupPoints : MonoBehaviour {

    public int scoreToGive = 100;
    private ScoreManager scoreManager;
    private AudioSource coinSound;

    void Start() {
        scoreManager = FindObjectOfType<ScoreManager>();
        coinSound = GameObject.Find("CoinSound").GetComponent<AudioSource>();
    }

    void Update() {
        
    }

    // Función nativa de unity. Si dos collider se juntan, se activa esta función
    void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.name == "Player") {
            scoreManager.AddScore(scoreToGive);
            gameObject.SetActive(false);

            if(coinSound.isPlaying){
                coinSound.Stop();
                coinSound.Play();
            } else {
                coinSound.Play();
            }
        }
    }
}
