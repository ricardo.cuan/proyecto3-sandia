using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour {

    public bool doublePoints;
    public bool safeMode;
    public float powerupLength;

    public Sprite[] powerupSprites;

    private AudioSource powerUpAudio;
    private PowerupManager powerupManager;

    void Start() {
        powerupManager = FindObjectOfType<PowerupManager>();
        powerUpAudio = GameObject.Find("PowerUpSound").GetComponent<AudioSource>();
    }

    void Update() {
        
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(other.name == "Player") {
            powerupManager.ActivatePowerup(doublePoints, safeMode, powerupLength);
            powerUpAudio.Play();
        }
        gameObject.SetActive(false);
    }
}
