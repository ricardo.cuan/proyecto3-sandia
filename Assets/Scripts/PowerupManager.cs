using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour {

    private bool doublePoints;
    private bool safeMode;

    private bool powerupActive;
    private float powerupLengthCounter;

    private ScoreManager scoreManager;
    private PlatformGenerator platformGenerator;
    private GameManager gameManager;

    private float normalPointsPerSecond;
    private float spikeRate;

    private PlatformDestroyer[] spikeList;

    void Start() {
        scoreManager = FindObjectOfType<ScoreManager>();
        platformGenerator = FindObjectOfType<PlatformGenerator>();
        gameManager = FindObjectOfType<GameManager>();
    }

    void Update() {
        if(powerupActive) {
            powerupLengthCounter -= Time.deltaTime;

            if(gameManager.powerupReset){
                powerupLengthCounter = 0;
                gameManager.powerupReset = false;
            }

            if(doublePoints){
                scoreManager.pointsPerSecond = normalPointsPerSecond * 2f;
                scoreManager.shouldDouble = true;
            }
            if(safeMode){
                platformGenerator.randomSpikeTreshold = 0;
            }

            if(powerupLengthCounter <= 0) {
                scoreManager.pointsPerSecond = normalPointsPerSecond;
                scoreManager.shouldDouble = false;

                platformGenerator.randomSpikeTreshold = spikeRate;
                powerupActive = false;
            }
        }
    }

    public void ActivatePowerup(bool points, bool safe, float time) {
        doublePoints = points;
        safeMode = safe;
        powerupLengthCounter = time;

        normalPointsPerSecond = scoreManager.pointsPerSecond;
        spikeRate = platformGenerator.randomSpikeTreshold;

        if(safeMode){
            spikeList = FindObjectsOfType<PlatformDestroyer>();
            for(int i = 0; i < spikeList.Length; i++) {
                if(spikeList[i].gameObject.name.Contains("Spike")) {
                    spikeList[i].gameObject.SetActive(false);
                }
            }
        }

        powerupActive = true;
    }
}
