using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody2D player;

    // Velociddad
    public float speed = 5f;
    private float speedStore;
    public float speedMultiplier = 1.05f;
    public float speedIncreaseLevel = 50f;
    private float speedIncreaseLevelStore;
    public float speedLevelCount;
    private float speedLevelCountStore;

    // Salto
    public float jump = 15f;
    public float jumpTime = 0.25f;
    public Transform groundCheck;
    public LayerMask GroundLayer;
    public float groundCheckRadius = 0.1f;
    private bool isGround;
    private float jumpTimeCounter;

    // Game
    public GameManager gameManager;

    // Animator
    private Animator playerAnimator;

    public AudioSource jumpSound;
    public AudioSource deathSound;

    // Start is called before the first frame update
    void Start() {
        player = GetComponent<Rigidbody2D>();

        speedLevelCount = speedIncreaseLevel;
        speedStore = speed;
        speedLevelCountStore = speedLevelCount;
        speedIncreaseLevelStore = speedIncreaseLevel;

        jumpTimeCounter = jumpTime;

        playerAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        // Desplazamiento horizontal constante
        player.velocity = new Vector2(speed, player.velocity.y);

        // El jugador sube de nivel de velocidad
        if(transform.position.x > speedLevelCount) {
            speedLevelCount += speedIncreaseLevel;
            speedIncreaseLevel *= speedMultiplier;
            speed = speed * speedMultiplier;
        }

        // Salto
        isGround = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, GroundLayer);
        if( Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) ){
            if(isGround){
                player.velocity = new Vector2(player.velocity.x, jump);
                jumpSound.Play();
            }
        }

        // Salto prolongado al dejar presionado el salto
        if( Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0) ) {
            if(jumpTimeCounter > 0) {
                player.velocity = new Vector2(player.velocity.x, jump);
                jumpTimeCounter -= Time.deltaTime;
            }
        }
        if(Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0)){
            jumpTimeCounter = 0;
        }
        if(isGround) {
            jumpTimeCounter = jumpTime;
        }

        // Animator
        playerAnimator.SetFloat("speed", player.velocity.x);
        playerAnimator.SetBool("isGround", isGround);
    }

    // Muerte - Pela bollo
    // Este es una función nativa de Unity. Se usa para detección de collisiones
    void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "killbox") {
            gameManager.RestartGame();

            // Resetear la velocidad
            speed = speedStore;
            speedLevelCount = speedLevelCountStore;
            speedIncreaseLevel = speedIncreaseLevelStore;
            deathSound.Play();
        }
    }
}
