using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour {


    public ObjectPooler objectPool;
    public ObjectPooler[] objectPools;
    public Transform generationPoint;
    public float distanceBetweenMin = 0.5f;
    public float distanceBetweenMax = 3f;

    private float[] platformWidths;
    private float distanceBetween;
    private float flatPlatformWidth;
    private int platformSelector;
    private ScoreManager scoreManager;

    private CoinGenerator coinGenerator;
    public float randomNumber;
    public float[] randomCoinTreshold = {0.5f, 0.7f, 0.9f}; // [1Coin, 2Coin, 3Coin]

    public float randomSpikeTreshold;
    public ObjectPooler spikePool;

    public float powerupHeight = 3f;
    public float powerupThreshold = 50f;
    public ObjectPooler powerupPool;
    public ObjectPooler powerupPool2;

    void Start() {
        flatPlatformWidth = objectPool.pooledObject.GetComponent<BoxCollider2D>().size.x;

        platformWidths = new float[objectPools.Length];
        for (int i = 0; i < objectPools.Length; i++) {
            platformWidths[i] = objectPools[i].pooledObject.GetComponent<BoxCollider2D>().size.x;
        }

        scoreManager = FindObjectOfType<ScoreManager>();
        coinGenerator = FindObjectOfType<CoinGenerator>();
    }

    void Update() {
        if( transform.position.x < generationPoint.position.x ) {
            distanceBetween = Random.Range( distanceBetweenMin, distanceBetweenMax );
            platformSelector = Random.Range(0, objectPools.Length);

            // Mover el objeto actual a la nueva posición
            if (scoreManager.level == 1) {

                // Generación de PowerUps
                if(Random.Range(0f,100f) < powerupThreshold) {
                    GameObject newPowerup = powerupPool.GetPooledObject();

                    newPowerup.transform.position = transform.position + new Vector3(distanceBetween / 2f, Random.Range(1f, powerupHeight), 0f);
                    newPowerup.SetActive(true);
                }

                // Generación de PowerUps
                if(Random.Range(0f,100f) < powerupThreshold) {
                    GameObject newPowerup2 = powerupPool2.GetPooledObject();

                    newPowerup2.transform.position = transform.position + new Vector3(distanceBetween / 2f, Random.Range(1f, powerupHeight), 0f);
                    newPowerup2.SetActive(true);
                }

                transform.position = new Vector3(
                    transform.position.x + flatPlatformWidth,
                    transform.position.y,
                    transform.position.z
                );

                GameObject newFlatPlatform = objectPool.GetPooledObject();

                GenerateCoin();

                newFlatPlatform.transform.position = transform.position;
                newFlatPlatform.transform.rotation = transform.rotation;
                newFlatPlatform.SetActive(true);
            }

            // Segundo nivel
            if (scoreManager.level >= 2) {

                // Generación de PowerUps
                if(Random.Range(0f,100f) < powerupThreshold) {
                    GameObject newPowerup = powerupPool.GetPooledObject();

                    newPowerup.transform.position = transform.position + new Vector3(distanceBetween / 2f, powerupHeight, 0f);
                    newPowerup.SetActive(true);
                }

                if(Random.Range(0f,100f) < powerupThreshold) {
                    GameObject newPowerup2 = powerupPool2.GetPooledObject();

                    newPowerup2.transform.position = transform.position + new Vector3(distanceBetween / 2f, powerupHeight, 0f);
                    newPowerup2.SetActive(true);
                }

                transform.position = new Vector3(
                    transform.position.x + platformWidths[platformSelector]/2 + distanceBetween,
                    transform.position.y,
                    transform.position.z
                );
                // Activar una nueva plataforma del pool de objeto
                GameObject newPlatform = objectPools[platformSelector].GetPooledObject();

                // Activar la plataforma
                newPlatform.transform.position = transform.position;
                newPlatform.transform.rotation = transform.rotation;
                newPlatform.SetActive(true);

                // Moneda y Spike aparecen a la vez
                GenerateCoin();
                GenerateSpike();

                // Colocar el centro de la plataforma al final
                transform.position = new Vector3(
                    transform.position.x + platformWidths[platformSelector]/2,
                    transform.position.y,
                    transform.position.z
                );
            }

            // Instantiate( /*platform*/ platforms[platformSelector], transform.position, transform.rotation );
        }
    }

    private void GenerateSpike(){
        if(Random.Range(0f, 1f) < randomSpikeTreshold) {
            GameObject newSpike = spikePool.GetPooledObject();

            float spikeXPosition = Random.Range(
                -platformWidths[platformSelector] / 2f + 1f,
                platformWidths[platformSelector] / 2f - 1f
            );

            Vector3 spikePosition = new Vector3(spikeXPosition, 0.5f, 0f);

            newSpike.transform.position = transform.position + spikePosition;
            newSpike.transform.rotation = transform.rotation;
            newSpike.SetActive(true);
        }
    }

    private void GenerateCoin(){
        // GENERACIÓN DE MONEDAS
        randomNumber = Random.Range(0f,1f);

        // 3 Monedas
        if(randomNumber > randomCoinTreshold[2]) {
            coinGenerator.SpawnThreeCoins(new Vector3(
                transform.position.x,
                transform.position.y + 1f,
                transform.position.z
            ));
            return;
        }

        // 2 Monedas
        if(randomNumber > randomCoinTreshold[1]) {
            coinGenerator.SpawnTwoCoins(new Vector3(
                transform.position.x,
                transform.position.y + 1f,
                transform.position.z
            ));
            return;
        }

        // 1 Moneda
        if( randomNumber > randomCoinTreshold[0]) {
            coinGenerator.SpawnOneCoins(new Vector3(
                transform.position.x,
                transform.position.y + 1f,
                transform.position.z
            ));
            return;
        }

        if (scoreManager.level == 1) {
            if(0.1f < randomSpikeTreshold) {
                GameObject newSpike = spikePool.GetPooledObject();

                float spikeXPosition = Random.Range(
                    -flatPlatformWidth / 2f + 1f,
                    flatPlatformWidth / 2f - 1f
                );

                Vector3 spikePosition = new Vector3(spikeXPosition, 0.5f, 0f);

                newSpike.transform.position = transform.position + spikePosition;
                newSpike.transform.rotation = transform.rotation;
                newSpike.SetActive(true);
            }
        }
    }
}
